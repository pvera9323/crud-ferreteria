<?php 
    include("conexion.php");
    $con=conectar();

    $sql="SELECT *  FROM ferreteria";
    $query=mysqli_query($con,$sql);

    $row=mysqli_fetch_array($query);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Sucursales</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        
    </head>
    <body>
    <body style=" background: url('imagen/ferreteria.jpg') no-repeat;  background-size:cover;">
    
            <div class="container mt-5">
                    <div class="row"> 
                        
                        <div class="col-md-3">
                            <h1>Ingrese los datos de su Ferreteria</h1>
                                <form action="insertar.php" method="POST">

                                    <input type="text" class="form-control mb-3" name="ID_FERRETERIA" placeholder="Codigo de la Ferreteria">
                                    <input type="text" class="form-control mb-3" name="NOM_FERRETERIA" placeholder="Nombre de la ferreteria">
                                    <input type="text" class="form-control mb-3" name="DIR_FERRETERIA" placeholder="Dirección de la Ferreteria">
                                    <input type="text" class="form-control mb-3" name="TEL_FERRETERIA" placeholder="Telefono">
                                    <input type="submit" class="btn btn-primary">
                                </form>
                        </div>

                        <div class="col-md-8">
                            <table class="table" >
                                <thead class="table table-dark table-striped" >     
                                    <tr>
                                        <th>Codigo de la Ferreteria</th>
                                        <th>Nombre de la Ferreteria</th>
                                        <th>Dirección de la Ferreteria</th>
                                        <th>Telefono</th>
                                        <th></th>
                                        <th></th>
                                        <th>accion</th>
                                    </tr>
                                </thead>

                                <tbody>
                                        <?php
                                            while($row=mysqli_fetch_array($query)){
                                        ?>
                                            <tr>
                                                <th><?php  echo $row['ID_FERRETERIA']?></th>
                                                <th><?php  echo $row['NOM_FERRETERIA']?></th>
                                                <th><?php  echo $row['DIR_FERRETERIA']?></th>
                                                <th><?php  echo $row['TEL_FERRETERIA']?></th> 
                                                <th> </th>
                                                <th><a href="actualizar.php?id=<?php echo $row['ID_FERRETERIA'] ?>" class="btn btn-info">Actualizar</a></th></th>
                                                <th><a href="delete.php?id=<?php echo $row['ID_FERRETERIA'] ?>" class="btn btn-danger">Eliminar</a></th>                                        
                                            </tr>
                                        <?php 
                                            }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>  
            </div>
    </body>
</html>