<?php 
    include("conexion.php");
    $con=conectar();

$id=$_GET['id'];

$sql="SELECT * FROM ferreteria WHERE ID_FERRETERIA='$id'";
$query=mysqli_query($con,$sql);

$row=mysqli_fetch_array($query);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <title>Actualizar</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        
    </head>
    <body>
    <body style=" background: url('imagen/ferreteria.jpg') no-repeat;  background-size:cover;">
                <div class="container mt-5">
                <h1>Actualizar datos</h1>
                    <form action="update.php" method="POST">
                                <input type="hidden" name="ID_FERRETERIA" value="<?php echo $row['ID_FERRETERIA']  ?>">
                                <input type="text" class="form-control mb-3" name="NOM_FERRETERIA" placeholder="Actualice el nombre de la Ferreteria" value="<?php echo $row['NOM_FERRETERIA']  ?>">
                                <input type="text" class="form-control mb-3" name="DIR_FERRETERIA" placeholder="Actualice dirección" value="<?php echo $row['DIR_FERRETERIA']  ?>">
                                <input type="text" class="form-control mb-3" name="TEL_FERRETERIA" placeholder="Actualice su telefono" value="<?php echo $row['TEL_FERRETERIA']  ?>">
                            <input type="submit" class="btn btn-primary btn-block" value="Actualizar">
                    </form>
                    
                </div>
    </body>
</html>